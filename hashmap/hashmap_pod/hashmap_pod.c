/*
 * hashmap_pod.c
 *
 * Copyright (c) 2013, Sebastian Portillo <msp0111@famaf.unc.edu.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 */

#include <assert.h>

#include <hashmap/hashmap_pod/hashmap_pod.h>
#include <hashmap/hashmap_pod/my_hashmap_pod_defaults.h>
#include <vector/vector_pod_macro/vector_pod_macro.h>

/*
 * TODO: The 'size' parameter in the constructor and the 'resize' method
 * should refer to the size of the store. That would allow one to assert that
 * the table will not realloc before being able to hold up to size elements.
 * (This of course while max_load and grow_factor remain unchanged.)
 * TODO: Add fit (remove and rehash, and shrink vectors?)
 * TODO: It was fun thinking about it but maybe you should just drop the tricks
 * and use floor() and ceil().
 * TODO: Add iterators (avoid looking for the same entry several times).
 * TODO: Change at and find so that they also take const key_type*.
 */

#define ENTRY_INITIALIZER(KEY, VALUE) { KEY, VALUE, false, NULL }

/*** CODE BEGIN ***/

typedef struct entry* entry_t;

struct entry {
    key_type key;
    value_type val;
    bool erased;
    entry_t next;
};

DECLARE_STATIC_VECTOR_POD(entry, struct entry)
DECLARE_STATIC_VECTOR_POD(entryptr, entry_t)

struct hashmap_pod {
    vector_entryptr_t buckets;
    vector_entry_t store;
    float max_load;
    float grow_factor;
    size_t erased_count;
    hashmap_pod_hash_f hash;
    hashmap_pod_key_eq_f key_eq;
};

static const float kHashmapPodDefaultMaxLoad = 0.7;
static const float kHashmapPodDefaultGrowFactor = 1.6;
static const key_type kHashmapPodDefaultKey = kMyHashmapPodDefaultKey;
static const value_type kHashmapPodDefaultValue = kMyHashmapPodDefaultValue;
static const hashmap_pod_key_eq_f kHashmapPodKeyEq = kMyHashmapPodKeyEq;

IMPLEMENT_VECTOR_POD(entry, struct entry, VGP_ADHOC, 1.6,
       ENTRY_INITIALIZER(kMyHashmapPodDefaultKey, kMyHashmapPodDefaultValue))
IMPLEMENT_VECTOR_POD(entryptr, entry_t, VGP_ADHOC, 1.6, NULL)

static hashmap_pod_t hashmap_pod_create_common(size_t buckets_size,
        float max_load, float grow_factor, hashmap_pod_hash_f hash,
        hashmap_pod_key_eq_f key_eq);
static entry_t hashmap_pod_lookup(hashmap_pod_t hm, key_type key);
static void hashmap_pod_remove_and_rehash(hashmap_pod_t hm);

/*
 * Setup members: buckets, store, max_load, grow_factor, erased, hash, key_eq.
 * Note that this function assumes that all arguments have already been
 * verified and are sane.
 */
static hashmap_pod_t hashmap_pod_create_common(size_t buckets_size,
        float max_load, float grow_factor, hashmap_pod_hash_f hash,
        hashmap_pod_key_eq_f key_eq)
{
    hashmap_pod_t hm = NULL;

    hm = malloc(sizeof(*hm));
    if (hm == NULL)
        return NULL;

    hm->max_load = max_load;
    hm->grow_factor = grow_factor;
    hm->erased_count = 0;
    hm->hash = hash;
    hm->key_eq = key_eq;

    hm->buckets = vector_entryptr_create(buckets_size);
    if (hm->buckets == NULL) {
        free(hm);
        return NULL;
    }

    /*
     * fill with empty buckets; notice that resize won't fail since we already
     * reserved the required memory
     */
    vector_entryptr_resize(hm->buckets, buckets_size);

    hm->store = vector_entry_create(buckets_size*hm->max_load);
    if (hm->store == NULL) {
        vector_entryptr_destroy(hm->buckets);
        free(hm);
        return NULL;
    }

    return hm;
}

hashmap_pod_t hashmap_pod_create(hashmap_pod_hash_f hash, size_t size)
{
    hashmap_pod_t hm = NULL;
    float lboundf;

    assert(hash != NULL);
    assert(0 < kHashmapPodDefaultMaxLoad);
    assert(1 < kHashmapPodDefaultGrowFactor);
    assert(kHashmapPodKeyEq != NULL);

    /*
     * in the following, note that the actual lower bound is ceil(lboundf) but,
     * ceil(lboundf) <= floor(lboundf + 1)
     * and,
     * lboundf > 0 => trunc(lboundf + 1) = floor(lboundf + 1)
     */
    lboundf = (1 + 1/kHashmapPodDefaultMaxLoad) /
                                (kHashmapPodDefaultGrowFactor - 1);
    if (size < lboundf)
        size = lboundf + 1;

    hm = hashmap_pod_create_common(size, kHashmapPodDefaultMaxLoad,
            kHashmapPodDefaultGrowFactor, hash, kHashmapPodKeyEq);
    if (hm == NULL)
        return NULL;

    return hm;
}

void hashmap_pod_destroy(hashmap_pod_t hm)
{
    assert(hm != NULL);

    vector_entryptr_destroy(hm->buckets);
    vector_entry_destroy(hm->store);
    free(hm);
}

hashmap_pod_t hashmap_pod_copy(hashmap_pod_t hm)
{
    hashmap_pod_t copy = NULL;
    entry_t e = NULL;
    size_t hm_store_size;
    size_t i;

    assert(hm != NULL);

    copy = hashmap_pod_create_common(vector_entryptr_size(hm->buckets),
            hm->max_load, hm->grow_factor, hm->hash, hm->key_eq);
    if (copy == NULL)
        return NULL;

    hm_store_size = vector_entry_size(hm->store);

    for (i = 0; i < hm_store_size; ++i) {
        e = vector_entry_at(hm->store, i);
        if (!e->erased)
            vector_entry_push_back(copy->store, *e); /* assuming copyable pod */
    }

    hashmap_pod_remove_and_rehash(copy);

    return copy;
}

void hashmap_pod_set_max_load(hashmap_pod_t hm, float max_load)
{
    float lbound;

    assert(hm != NULL);
    assert(0 < max_load);

    lbound = 1/(vector_entryptr_size(hm->buckets)*(hm->grow_factor - 1) - 1);

    if (max_load < lbound)
        hm->max_load = lbound;
    else
        hm->max_load = max_load;
}

float hashmap_pod_get_max_load(hashmap_pod_t hm)
{
    assert(hm != NULL);

    return hm->max_load;
}

void hashmap_pod_set_grow_factor(hashmap_pod_t hm, float grow_factor)
{
    float lbound;
    size_t buckets_size;

    assert(hm != NULL);
    assert(1 < grow_factor);

    buckets_size = vector_entryptr_size(hm->buckets);
    lbound = 1 + 1/buckets_size + 1/(buckets_size*hm->max_load);

    if (grow_factor < lbound)
        hm->grow_factor = lbound;
    else
        hm->grow_factor = grow_factor;
}

float hashmap_pod_get_grow_factor(hashmap_pod_t hm)
{
    assert(hm != NULL);

    return hm->grow_factor;
}

/**
 * Lookup an entry with key 'key' in the hashmap and return a pointer to it
 * (even if it's marked as erased). If such entry does not exist return NULL.
 *
 * Pre: hm != NULL
 */
static entry_t hashmap_pod_lookup(hashmap_pod_t hm, key_type key)
{
    entry_t e = NULL;
    size_t buckets_size;
    size_t ix;

    assert(hm != NULL);

    buckets_size = vector_entryptr_size(hm->buckets);
    ix = hm->hash(&key) % buckets_size;

    /* see if key already exists */
    for (e = *vector_entryptr_at(hm->buckets, ix); e != NULL; e = e->next)
        if (hm->key_eq(&e->key, &key))
            return e;

    /* key is not in the table */

    return NULL;
}

value_type* hashmap_pod_find(hashmap_pod_t hm, key_type key)
{
    entry_t e = NULL;

    assert(hm != NULL);

    e = hashmap_pod_lookup(hm, key);
    return e && !e->erased ? &e->val : NULL;
}

value_type* hashmap_pod_at(hashmap_pod_t hm, key_type key)
{
    entry_t e = NULL;
    entry_t* bucket_ptr = NULL;
    struct entry new_entry;
    size_t buckets_size;
    size_t store_size;
    size_t ix;

    assert(hm != NULL);

    buckets_size = vector_entryptr_size(hm->buckets);
    store_size = vector_entry_size(hm->store);
    ix = hm->hash(&key) % buckets_size;

    /* see if key already exists */
    for (e = *vector_entryptr_at(hm->buckets, ix); e != NULL; e = e->next) {
        if (hm->key_eq(&e->key, &key)) {
            if (e->erased) {
                e->erased = false;
                e->val = kHashmapPodDefaultValue;
                --hm->erased_count;
            }
            return &e->val;
        }
    }

    /* key is not in the table */

    /*
     * table too full? ie, need to resize?
     * note: this test could be replaced with,
     * vector_entry_size(hm->store) == vector_entry_capacity(hm->store)
     */
    if (buckets_size*hm->max_load < store_size + 1) {
        if (hashmap_pod_resize(hm, buckets_size*hm->grow_factor))
            return hashmap_pod_at(hm, key);
        else
            return NULL;  /* failed to resize; TODO: this sucks too much */
    }

    /* add key to the start of the bucket */
    new_entry.key = key;
    new_entry.val = kHashmapPodDefaultValue;
    new_entry.erased = false;
    bucket_ptr = vector_entryptr_at(hm->buckets, ix);
    new_entry.next = *bucket_ptr;
    e = vector_entry_push_back(hm->store, new_entry);
    *bucket_ptr = e;

    return &e->val;
}

/**
 * Actually remove erased elements and rehash the whole hashmap.
 *
 * Pre: hm != NULL
 */
static void hashmap_pod_remove_and_rehash(hashmap_pod_t hm)
{
    entry_t elem = NULL;
    entry_t* bucket_ptr = NULL;
    size_t bucket_ix;
    size_t buckets_size;
    size_t store_size;
    size_t i;

    assert(hm != NULL);

    /*
     * fill the whole vector with empty buckets (NULL pointers)
     * note that we need to clear first so that resize touches all elements
     */
    vector_entryptr_clear(hm->buckets);
    vector_entryptr_resize(hm->buckets, vector_entryptr_capacity(hm->buckets));

    buckets_size = vector_entryptr_size(hm->buckets);
    store_size = vector_entry_size(hm->store);

    /* actually remove marked entries */
    if (store_size == hm->erased_count) {
        vector_entry_clear(hm->store);
        hm->erased_count = 0;
        return;
    }
    for (i = store_size - 1; /*0 <= i &&*/ hm->erased_count != 0; --i) {
        if (vector_entry_at(hm->store, i)->erased) {
            vector_entry_erase(hm->store, i);
            --hm->erased_count;
        }
    }

    /* rehash */
    store_size = vector_entry_size(hm->store);
    for (i = 0; i < store_size; ++i) {
        elem = vector_entry_at(hm->store, i);
        bucket_ix = hm->hash(&elem->key) % buckets_size;
        bucket_ptr = vector_entryptr_at(hm->buckets, bucket_ix);
        /* link elements in the same bucket */
        elem->next = *bucket_ptr;
        *bucket_ptr = elem;
    }
}

bool hashmap_pod_resize(hashmap_pod_t hm, size_t new_size)
{
    assert(hm != NULL);

    if (new_size <= vector_entryptr_size(hm->buckets))
        return true;  /* false is reserved for oom situations */

    /* see if we can have all the required memory */
    if (!vector_entryptr_reserve(hm->buckets, new_size))
        return false;
    /* if needed, reallocate the store now */
    if (!vector_entry_reserve(hm->store, hm->max_load*new_size)) {
        vector_entryptr_fit(hm->buckets); /* waive memory back */
        return false;
    }

    hashmap_pod_remove_and_rehash(hm);

    return true;
}

void hashmap_pod_erase(hashmap_pod_t hm, key_type key)
{
    entry_t e = NULL;

    assert(hm != NULL);

    e = hashmap_pod_lookup(hm, key);
    if (e != NULL && !e->erased) {
        e->erased = true;
        ++hm->erased_count;
    }
}

void hashmap_pod_clear(hashmap_pod_t hm)
{
    entry_t e = NULL;
    size_t store_size;
    size_t i;

    assert(hm != NULL);

    store_size = vector_entry_size(hm->store);

    for (i = 0; i < store_size; ++i) {
        e = vector_entry_at(hm->store, i);
        if (!e->erased) {
            e->erased = true;
            ++hm->erased_count;
        }
    }
}

void hashmap_pod_compact(hashmap_pod_t hm)
{
    assert(hm != NULL);

    hashmap_pod_remove_and_rehash(hm);
}

size_t hashmap_pod_size(hashmap_pod_t hm)
{
    assert(hm != NULL);

    return vector_entry_size(hm->store) - hm->erased_count;
}

size_t hashmap_pod_buckets(hashmap_pod_t hm)
{
    assert(hm != NULL);

    return vector_entryptr_size(hm->buckets);
}

void hashmap_pod_foreach(hashmap_pod_t hm, hashmap_pod_foreach_f f)
{
    entry_t e = NULL;
    size_t i;

    assert(hm != NULL);
    assert(f != NULL);

    for (i = 0; i < vector_entry_size(hm->store); ++i) {
        e = vector_entry_at(hm->store, i);
        if (!e->erased)
            f(&e->key, &e->val);
    }
}

void hashmap_pod_foreach_data(hashmap_pod_t hm, hashmap_pod_foreach_data_f f,
        void* data)
{
    entry_t e = NULL;
    size_t i;

    assert(hm != NULL);
    assert(f != NULL);

    for (i = 0; i < vector_entry_size(hm->store); ++i) {
        e = vector_entry_at(hm->store, i);
        if (!e->erased)
            f(&e->key, &e->val, data);
    }
}

void hashmap_pod_foreach_func(hashmap_pod_t hm, hashmap_pod_foreach_func_f f,
        void(*func)())
{
    entry_t e = NULL;
    size_t i;

    assert(hm != NULL);
    assert(f != NULL);

    for (i = 0; i < vector_entry_size(hm->store); ++i) {
        e = vector_entry_at(hm->store, i);
        if (!e->erased)
            f(&e->key, &e->val, func);
    }
}

/**
 * Return true if the hashmap is compact: in the store, the first non-erased
 * element is at position 0 and all non-erased elements are contiguous (i.e.
 * the store has no "holes"). Otherwise return false.
 *
 * Pre: hm != NULL
 */
static bool hashmap_pod_verify_for_iterator(hashmap_pod_t hm)
{
    size_t i, existent = 0;

    assert(hm != NULL);

    for (i = 0; i < vector_entry_size(hm->store); ++i)
        if (vector_entry_at(hm->store, i)->erased)
            break;
        else
            ++existent;

    return existent == hashmap_pod_size(hm);
}

hashmap_pod_iterator_t hashmap_pod_iterator_begin(hashmap_pod_t hm)
{
    assert(hm != NULL);
    assert(hashmap_pod_verify_for_iterator(hm));

    (void)hm;

    return 0;
}

bool hashmap_pod_iterator_end(hashmap_pod_t hm, hashmap_pod_iterator_t it)
{
    assert(hm != NULL);
    assert(hashmap_pod_verify_for_iterator(hm));

    return hashmap_pod_size(hm) <= it;
}

hashmap_pod_iterator_t hashmap_pod_iterator_next(hashmap_pod_t hm,
        hashmap_pod_iterator_t it)
{
    assert(hm != NULL);
    assert(hashmap_pod_verify_for_iterator(hm));

    return hashmap_pod_size(hm) <= it ? it : ++it;
}

value_type* hashmap_pod_iterator_at(hashmap_pod_t hm,
        hashmap_pod_iterator_t it)
{
    assert(hm != NULL);
    assert(hashmap_pod_verify_for_iterator(hm));
    assert(!hashmap_pod_iterator_end(hm, it));

    return &vector_entry_at(hm->store, it)->val;
}

const key_type* hashmap_pod_iterator_key(hashmap_pod_t hm,
        hashmap_pod_iterator_t it)
{
    assert(hm != NULL);
    assert(hashmap_pod_verify_for_iterator(hm));
    assert(!hashmap_pod_iterator_end(hm, it));

    return &vector_entry_at(hm->store, it)->key;
}

/*** CODE END ***/
