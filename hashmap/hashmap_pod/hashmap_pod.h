/*
 * hashmap_pod.h
 *
 * Copyright (c) 2013, Sebastian Portillo <msp0111@famaf.unc.edu.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 */

#ifndef _HASHMAP_POD_H_
#define _HASHMAP_POD_H_

#include <stdbool.h>
#include <stdlib.h>

#include <hashmap/hashmap_pod/my_hashmap_pod_types.h>

/*** CODE BEGIN ***/

/**
 * This is a specialization of 'hashmap' intended to be used with POD elements.
 * As such, contained element are not 'constructed' nor 'destroyed', and
 * copying is performed by simple byte mirroring.
 * When needed, for instance when inserting new elements, a user-configured
 * 'default' element is used.
 */

/**
 * Hash operator for keys in the hashmap.
 */
typedef size_t (*hashmap_pod_hash_f)(const key_type*);

/**
 * Equality operator for keys in the hashmap.
 */
typedef bool (*hashmap_pod_key_eq_f)(const key_type* k1, const key_type* k2);

/**
 * Callback function used when iterating over the hashmap, where,
 *   k      The elements' key.
 *   v      The elements' value.
 */
typedef void (*hashmap_pod_foreach_f)(const key_type* k, value_type* v);

/**
 * Callback function used when iterating over the hashmap, where,
 *   k      The elements' key.
 *   v      The elements' value.
 *   d      Opaque object pointer reserved for the user's contextual data.
 */
typedef void (*hashmap_pod_foreach_data_f)(const key_type* k, value_type* v,
        void* d);

/**
 * Callback function used when iterating over the hashmap, where,
 *   k      The elements' key.
 *   v      The elements' value.
 *   f      Opaque function pointer reserved for the user's contextual data.
 */
typedef void (*hashmap_pod_foreach_func_f)(const key_type* k, value_type* v,
        void(*f)());

typedef struct hashmap_pod* hashmap_pod_t;

/**
 * Constructor.
 * Construct a hashmap with hash operator 'hash' and initial size 'size'.
 * The 'size' parameter is constrained by other configuration paramenters (such
 * as maximum load factor and grow factor), the value provided by the user will
 * be automatically adjusted if necessary.
 * Note that this size currently refers to the table (the amount of 'buckets',
 * in hash table parlance) and not to the actual amount of elements that can
 * be stored without reallocation (which is lower).
 * This is a limitation of the current implementation.
 *
 * Pre: hash != NULL
 */
hashmap_pod_t hashmap_pod_create(hashmap_pod_hash_f hash, size_t size);

/**
 * Destructor.
 *
 * Pre: hm != NULL
 */
void hashmap_pod_destroy(hashmap_pod_t hm);

/**
 * Copy constructor.
 *
 * Pre: hm != NULL
 */
hashmap_pod_t hashmap_pod_copy(hashmap_pod_t hm);

/**
 * Set the hashmap's maximum load factor.
 * The maximum load factor controls the average amount of collisions per
 * element in the hashmap. It is constrained by other paramenters (such as
 * current table size and grow factor), the value provided by the user will be
 * automatically adjusted if necessary.
 *
 * Pre: hm != NULL && 0 < max_load
 */
void hashmap_pod_set_max_load(hashmap_pod_t hm, float max_load);

/**
 * Return the hashmap's maximum load factor.
 * The maximum load factor controls the average amount of collisions per
 * element in the hashmap.
 *
 * Pre: hm != NULL && 0 < hashmap_pod_get_max_load(hm)
 */
float hashmap_pod_get_max_load(hashmap_pod_t hm);

/**
 * Set the hashmap's grow factor.
 * When the hashmap needs to grow it does so by this factor. It is constrained
 * by other paramenters (such as current table size and maximum load factor),
 * the value provided by the user will be automatically adjusted if necessary.
 *
 * Pre: hm != NULL && 1 < grow_factor
 */
void hashmap_pod_set_grow_factor(hashmap_pod_t hm, float grow_factor);

/**
 * Return the hashmap's grow factor.
 * When the hashmap needs to grow it does so by this factor.
 *
 * Pre: hm != NULL && 1 < hashmap_pod_get_grow_factor(hm);
 */
float hashmap_pod_get_grow_factor(hashmap_pod_t hm);

/**
 * Return a pointer to the value associated with key 'key' in the hashmap, if
 * such key exists. Otherwise return NULL.
 *
 * Pre: hm != NULL
 */
value_type* hashmap_pod_find(hashmap_pod_t hm, key_type key);

/**
 * Return a pointer to the value associated with key 'key' in the hashmap. If
 * such key did not exist, insert it associated with a default value and return
 * a pointer to this.
 * Note that this operation can resize the hashmap, potentially invalidating
 * other pointers into the hashmap.
 * Return NULL if a resize operation failed.
 *
 * Pre: hm != NULL
 */
value_type* hashmap_pod_at(hashmap_pod_t hm, key_type key);

/**
 * Change the hashmap's size.
 * This function can only increase the size.
 * Note that this size currently refers to the table (the amount of 'buckets',
 * in hash table parlance) and not to the actual amount of elements that can
 * be stored without reallocation (which is lower).
 * This is a limitation of the current implementation.
 * Note that this a potentially expensive operation.
 * Return true if successful, otherwise false.
 *
 * Pre: hm != NULL
 */
bool hashmap_pod_resize(hashmap_pod_t hm, size_t new_size);

/**
 * Remove element with key 'key'.
 * Note: actual removal is deferred.
 *
 * Pre: hm != NULL
 */
void hashmap_pod_erase(hashmap_pod_t hm, key_type key);

/**
 * Remove all elements from the hashmap.
 * Note: actual removal is deferred.
 *
 * Pre: hm != NULL
 * Post: hashmap_pod_size(hm) == 0
 */
void hashmap_pod_clear(hashmap_pod_t hm);

/**
 * Actually remove erased elements from the hashmap.
 * Note that this function will not waive memory back to the system.
 * If you plan to reuse the same hashmap with a significatively different key
 * domain, this operation can be useful to reduce "harmful collisions".
 * If instead you plan to reuse it with the same or a significatively similar
 * key domain, then you would probably be better off by leaving it as is.
 * This behaviour results from the fact that the current implementation does
 * not actually remove elements when they are "erased", instead it just marks
 * them as such and defers a potentially costly operation until a resize or
 * fit.
 *
 * Pre: hm != NULL
 */
void hashmap_pod_compact(hashmap_pod_t hm);

/**
 * Return the amount of elements in the hashmap.
 *
 * Pre: hm != NULL
 */
size_t hashmap_pod_size(hashmap_pod_t hm);

/**
 * Return the amount of buckets in the hashmap.
 * Debugging only.
 *
 * Pre: hm != NULL
 */
size_t hashmap_pod_buckets(hashmap_pod_t hm);

/**
 * Call 'f' for each element in the hashmap, in the order they were originally
 * added.
 *
 * Pre: hm != NULL
 */
void hashmap_pod_foreach(hashmap_pod_t hm, hashmap_pod_foreach_f f);

/**
 * Call 'f' for each element in the hashmap, in the order they were originally
 * added. Pass 'data' along to 'f'.
 *
 * Pre: hm != NULL
 */
void hashmap_pod_foreach_data(hashmap_pod_t hm, hashmap_pod_foreach_data_f f,
        void* data);

/**
 * Call 'f' for each element in the hashmap, in the order they were originally
 * added. Pass 'func' along to 'f'.
 *
 * Pre: hm != NULL
 */
void hashmap_pod_foreach_func(hashmap_pod_t hm, hashmap_pod_foreach_func_f f,
        void(*func)());

/**
 * Forward iterators. Quick and dirty.
 *
 * These iterators allow for sequential iteration over the elements in the
 * hashmap, in the order they were originally added.
 *
 * An important feature: unlike pointer, these iterators are not invalidated
 * when the hashmap reallocates memory. Additionally an algorithm can add
 * elements to the hashmap while in the midst of an iteration, and have those
 * elements visited next (eventually) during the same iteration.
 *
 * An importat caveat wrt the current implementation: unlike pointers, these
 * iterators require the hashmap to be compact (not to have a non-erased
 * elements after an erased one, i.e. not to have holes).
 */

typedef size_t hashmap_pod_iterator_t;

/**
 * Return an iterator to the beginning.
 *
 * Pre: hm != NULL && hm doesn't have erased elements
 */
hashmap_pod_iterator_t hashmap_pod_iterator_begin(hashmap_pod_t hm);

/**
 * Return true if 'it' is at the end of the iteration; otherwise false.
 *
 * Pre: hm != NULL && hm doesn't have erased elements
 */
bool hashmap_pod_iterator_end(hashmap_pod_t hm, hashmap_pod_iterator_t it);

/**
 * Return an iterator to the next element. If the iterator is already at
 * the end, then return the same iterator.
 *
 * Pre: hm != NULL && hm doesn't have erased elements
 */
hashmap_pod_iterator_t hashmap_pod_iterator_next(hashmap_pod_t hm,
        hashmap_pod_iterator_t it);

/**
 * Return a pointer to the value of the element associated with iterator 'it'.
 *
 * Pre: hm != NULL && hm doesn't have erased elements &&
 *      !hashmap_pod_iterator_end(hm, it)
 */
value_type* hashmap_pod_iterator_at(hashmap_pod_t hm,
        hashmap_pod_iterator_t it);

/**
 * Return a pointer to the key of the element associated with iterator 'it'.
 *
 * Pre: hm != NULL && hm doesn't have erased elements &&
 *      !hashmap_pod_iterator_end(hm, it)
 */
const key_type* hashmap_pod_iterator_key(hashmap_pod_t hm,
        hashmap_pod_iterator_t it);

/*** CODE END ***/

#endif
