#ifndef _MY_HASHMAP_POD_DEFAULTS_H_
#define _MY_HASHMAP_POD_DEFAULTS_H_

#define kMyHashmapPodDefaultKey 0

#define kMyHashmapPodDefaultValue 0

bool kMyHashmapPodKeyEq(const uint32_t* a, const uint32_t* b)
{
    return *a == *b;
}

#endif
