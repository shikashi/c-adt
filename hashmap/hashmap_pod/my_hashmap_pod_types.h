#ifndef _MY_HASHMAP_POD_TYPES_H_
#define _MY_HASHMAP_POD_TYPES_H_

#include <stdint.h>

typedef uint32_t key_type;

typedef size_t value_type;

#endif
