#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <stdint.h>

#include <hashmap/hashmap_pod/hashmap_pod.h>

size_t my_hasher(const uint32_t* key)
{
    const char* c = (const char*) key;
    size_t res = 0;
    int i;
    for (i = sizeof(key_type); i; --i)
        res = (res*131) + *c++;
    return res;
}

void add(hashmap_pod_t h, uint32_t key, value_type val)
{
    *hashmap_pod_at(h, key) = val;
}

void print_elem(const uint32_t* key, value_type* val)
{
    printf("(%d => %ld) ", *key, *val);
}

void print_elems(hashmap_pod_t h)
{
    printf("# %ld (%ld) [ ", hashmap_pod_size(h), hashmap_pod_buckets(h));
    hashmap_pod_foreach(h, print_elem);
    printf("]\n");
}

void print_values_it(hashmap_pod_t h)
{
    value_type* val = NULL;
    hashmap_pod_iterator_t it = hashmap_pod_iterator_begin(h);

    printf("{ ");
    while (!hashmap_pod_iterator_end(h, it)) {
        val = hashmap_pod_iterator_at(h, it);
        printf("%ld ", *val);
        it = hashmap_pod_iterator_next(h, it);
    }
    printf("}\n");
}

void test1(void)
{
    value_type* val1 = NULL;
    hashmap_pod_t h0 = NULL;
    hashmap_pod_t h = hashmap_pod_create(my_hasher, 1);
    print_elems(h);

    add(h, 5241, 4);
    print_elems(h);

    add(h, 51, 13);
    print_elems(h);

    add(h, 12, 4123);
    print_elems(h);

    add(h, 91939, 545);
    print_elems(h);

    *hashmap_pod_at(h, 1) = 0;

    val1 = hashmap_pod_at(h, 5241);

    print_elems(h);

    assert(*val1 == 4);

    h0 = hashmap_pod_copy(h);
    print_elems(h0);

    add(h0, 29, 0);
    add(h0, 12, 0);
    print_elems(h0);

    print_values_it(h0);
    print_elems(h0);

    hashmap_pod_destroy(h0);
    hashmap_pod_destroy(h);
}

int main()
{
    test1();

    return 0;
}
