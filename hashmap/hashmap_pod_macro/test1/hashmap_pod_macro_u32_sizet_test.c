#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <stdint.h>

#include "hashmap_u32_sizet.h"

size_t my_hasher(const u32* key)
{
    const char* c = (const char*) key;
    size_t res = 0;
    int i;
    for (i = sizeof(u32); i; --i)
        res = (res*131) + *c++;
    return res;
}

void add(hashmap_u32_sizet_t h, u32 key, size_t val)
{
    *hashmap_u32_sizet_at(h, key) = val;
}

void print_elem(const u32* key, size_t* val)
{
    printf("(%d => %ld) ", *key, *val);
}

void print_elems(hashmap_u32_sizet_t h)
{
    printf("# %ld (%ld) [ ", hashmap_u32_sizet_size(h),
            hashmap_u32_sizet_buckets(h));
    hashmap_u32_sizet_foreach(h, print_elem);
    printf("]\n");
}

void test1(void)
{
    size_t* val1 = NULL;
    hashmap_u32_sizet_t h0 = NULL;
    hashmap_u32_sizet_t h = hashmap_u32_sizet_create(my_hasher, 1);
    print_elems(h);

    add(h, 5241, 4);
    print_elems(h);

    add(h, 51, 13);
    print_elems(h);

    add(h, 12, 4123);
    print_elems(h);

    add(h, 91939, 545);
    print_elems(h);

    *hashmap_u32_sizet_at(h, 1) = 0;

    val1 = hashmap_u32_sizet_at(h, 5241);

    print_elems(h);

    assert(*val1 == 4);

    h0 = hashmap_u32_sizet_copy(h);
    print_elems(h0);

    add(h0, 29, 0);
    add(h0, 12, 0);
    print_elems(h0);

    hashmap_u32_sizet_destroy(h0);
    hashmap_u32_sizet_destroy(h);
}

int main()
{
    test1();

    return 0;
}
