#include "hashmap_u32_sizet.h"

static bool u32_eq(const u32* a, const u32* b)
{
    return *a == *b;
}

IMPLEMENT_HASHMAP_POD(u32_sizet, u32, size_t, 0.7, 1.6, 0, 0, u32_eq)
