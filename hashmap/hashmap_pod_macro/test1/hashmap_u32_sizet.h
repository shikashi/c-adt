#ifndef _HASHMAP_U32_SIZET_H_
#define _HASHMAP_U32_SIZET_H_

#include <stdint.h>

#include <hashmap/hashmap_pod_macro/hashmap_pod_macro.h>

typedef uint32_t u32;

DECLARE_HASHMAP_POD(u32_sizet, u32, size_t)

#endif
