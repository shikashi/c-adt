# macroize.py
#
# Copyright (c) 2013, Sebastian Portillo <msp0111@famaf.unc.edu.ar>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# TODO: The configuration part is a mess, fix it. Maybe all those variables
# should be properties in a class?
# TODO: Implement breaking of long lines.

import re

##### ORIGINAL

original_name = 'vector'
original_dir  = '../vector'

original_header = '%s/%s.h' % (original_dir, original_name)
original_source = '%s/%s.c' % (original_dir, original_name)

code_begin_marker = '/*** CODE BEGIN ***/'
code_end_marker   = '/*** CODE END ***/'

##### MACROIZATION

macroized_name = '%s_macro' % original_name
macroized_file = '%s.h' % macroized_name

includes = {
    'sys': [],
    'lib': [],
    'local': []
}

declaration_macro_params    = ['NAME', 'value_type']
implementation_macro_params = ['NAME', 'value_type']

common_substitutions = [
    # names (functions, types)
    ('%s_' % original_name, '%s_##NAME##_' % macroized_name),

    # the structure
    ('struct %s' % original_name, 'struct %s_##NAME' % macroized_name)
]

decl_substitutions = [
    # function declarations, replaced after common substitutions
    (r'^.+ %s_.+\(.*\);$' % macroized_name, r'QUALIF \g<0>')
]

impl_substitutions = []

decl_helpers = []
impl_helpers = []

##### ALGORITHM

def open_guard():
    guard = '_%s_' % macroized_file.upper().replace('.', '_')
    print '#ifndef %s' % guard
    print '#define %s' % guard
    print


def close_guard():
    print '#endif'


def write_includes():
    for h in includes['sys']:
        print '#include <%s>' % h
    print

    for h in includes['lib']:
        print '#include <%s>' % h
    for h in includes['local']:
        print '#include "%s"' % h
    print


def write_license():
    license = """/*
 * %s
 *
 * Copyright (c) 2013, Sebastian Portillo <msp0111@famaf.unc.edu.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 */
"""
    print license % macroized_file

def write_notice():
    print '/*** MACHINE-GENERATED ***/'
    print


class code_iterator(object):
    def __init__(self, file_path, begin_marker, end_marker):
        self.path = file_path
        self.begin_marker = begin_marker
        self.end_marker = end_marker
        self.fileobj = file(self.path, 'rb')
        self.in_code = False

    def __del__(self):
        self.fileobj.close()

    def seek_to_beginning(self):
        if self.in_code:
            self.fileobj.seek(0, 0)
            self.in_code = False

        while not self.in_code:
            line = self.fileobj.readline()
            if len(line) == 0:
                raise Exception("Could not find beginning marker "
                    "in file '" + self.path + "'.")
            if line.startswith(self.begin_marker):
                self.in_code = True

        assert(self.in_code)

    def __iter__(self):
        self.seek_to_beginning()
        return self

    def next(self):
        if not self.in_code:
            raise StopIteration()

        line = self.fileobj.readline()
        if len(line) == 0 or line.startswith(self.end_marker):
            self.in_code = False
            raise StopIteration()

        return line.rstrip()  # remove LF


def line_continuator(line):
    if (len(line) >= 78):
        cont = ' \\'
    else:
        cont = ' '*(80 - len(line) - 1) + '\\'

    return cont


def write_declaration_macro():
    print '/*** DECLARATION MACRO BEGIN ***/'
    print

    for h in decl_helpers:
        print h
        print

    # helper empty macro
    print '#define NOQUALIF'
    print

    # normal non-static declare
    nonstatic = '#define DECLARE_%s(%s)' % (original_name.upper(),
                    ', '.join(declaration_macro_params))
    print nonstatic + line_continuator(nonstatic)
    print '\t__DECLARE_%s(%s)' % (original_name.upper(),
              ', '.join(declaration_macro_params + ['NOQUALIF']))
    print

    # static declare
    static = '#define DECLARE_STATIC_%s(%s)' % (original_name.upper(),
                 ', '.join(declaration_macro_params))
    print static + line_continuator(static)
    print '\t__DECLARE_%s(%s)' % (original_name.upper(),
              ', '.join(declaration_macro_params + ['static']))
    print

    # the actual general declare
    general = '#define __DECLARE_%s(%s)' % (original_name.upper(),
                ', '.join(declaration_macro_params + ['QUALIF']))
    print general + line_continuator(general)

    write_macro_code(original_header, common_substitutions, decl_substitutions)

    print '/*** DECLARATION MACRO END ***/'
    print


def write_implementation_macro():
    print '/*** IMPLEMENTATION MACRO BEGIN ***/'
    print

    for h in impl_helpers:
        print h
        print

    macro = '#define IMPLEMENT_%s(%s)' % (original_name.upper(),
                 ', '.join(implementation_macro_params))
    print macro + line_continuator(macro)

    write_macro_code(original_source, common_substitutions, impl_substitutions)

    print '/*** IMPLEMENTATION MACRO END ***/'
    print


def write_macro_code(filepath, *substitutions):
    code = code_iterator(filepath, code_begin_marker, code_end_marker)

    prev_line = None

    for line in code:
        if prev_line is not None:
            print prev_line + line_continuator(prev_line)

        for sub_list in substitutions:
            for pattern, repl in sub_list:
                line = re.sub(pattern, repl, line)
        prev_line = line

    if prev_line is not None:
        print prev_line  # last line without continuator


def write_macroized():
    write_license()
    open_guard()
    write_includes()
    write_notice()

    print code_begin_marker
    print

    write_declaration_macro()
    write_implementation_macro()

    print code_end_marker
    print

    close_guard()
