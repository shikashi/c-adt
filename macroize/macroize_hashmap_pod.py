import macroize as m

##### ORIGINAL

m.original_name = 'hashmap_pod'
m.original_dir  = '../hashmap/hashmap_pod'

m.original_header = '%s/%s.h' % (m.original_dir, m.original_name)
m.original_source = '%s/%s.c' % (m.original_dir, m.original_name)

##### MACROIZATION

m.macroized_name = 'hashmap'
m.macroized_file = '%s_macro.h' % m.original_name

m.includes = {
    'sys': ['stdlib.h', 'stdbool.h', 'assert.h'],
    'lib': ['vector/vector_pod_macro/vector_pod_macro.h'],
    'local': []
}

m.declaration_macro_params    = ['NAME', 'key_type', 'value_type']
m.implementation_macro_params = ['NAME', 'key_type', 'value_type',
    'DEFAULT_MAX_LOAD', 'DEFAULT_GROW_FACTOR', 'DEFAULT_KEY',
    'DEFAULT_VALUE', 'KEY_EQ']

m.common_substitutions = [
    # names (functions, types)
    ('%s_' % m.original_name, '%s_##NAME##_' % m.macroized_name),

    # the structure
    ('struct %s' % m.original_name, 'struct %s_##NAME' % m.macroized_name)
]

m.decl_substitutions = [
    # function declarations, replaced after common substitutions
    (r'^.+ %s_.+\(.*\);$' % m.macroized_name, r'QUALIF \g<0>')
]

m.impl_substitutions = [
    # entry
    (r'entry_', 'entry_##NAME##_'),
    (r'entryptr_', 'entryptr_##NAME##_'),
    (r'struct entry', 'struct entry_##NAME'),
    (r'VECTOR_POD\(entry,', 'VECTOR_POD(entry_##NAME,'),
    (r'VECTOR_POD\(entryptr,', 'VECTOR_POD(entryptr_##NAME,'),
    (r'ENTRY_INITIALIZER\(\w+, \w+\)',
      'ENTRY_INITIALIZER(KGRP(DEFAULT_KEY), KGRP(DEFAULT_VALUE))'),

    # the initializers for the 5 constants
    (r'kHashmapPodDefaultMaxLoad = [^\n]*;',
      'kHashmapPodDefaultMaxLoad = DEFAULT_MAX_LOAD;'),
    (r'kHashmapPodDefaultGrowFactor = [^\n]*;',
      'kHashmapPodDefaultGrowFactor = DEFAULT_GROW_FACTOR;'),
    (r'kHashmapPodDefaultKey = [^\n]*;',
      'kHashmapPodDefaultKey = DEFAULT_KEY;'),
    (r'kHashmapPodDefaultValue = [^\n]*;',
      'kHashmapPodDefaultValue = DEFAULT_VALUE;'),
    (r'kHashmapPodKeyEq = [^\n]*;',
      'kHashmapPodKeyEq = KEY_EQ;'),

    # the names of the 5 constants (notice the previous sub uses the old names
    # so this must be after)
    ('kHashmapPodDefaultMaxLoad', 'kHashmapPodDefaultMaxLoad_##NAME'),
    ('kHashmapPodDefaultGrowFactor', 'kHashmapPodDefaultGrowFactor_##NAME'),
    ('kHashmapPodDefaultKey', 'kHashmapPodDefaultKey_##NAME'),
    ('kHashmapPodDefaultValue', 'kHashmapPodDefaultValue_##NAME'),
    ('kHashmapPodKeyEq', 'kHashmapPodKeyEq_##NAME')
]

m.impl_helpers = [
    '#define KGRP(...) __VA_ARGS__',
    '#define ENTRY_INITIALIZER(KEY, VALUE) { KEY, VALUE, false, NULL }'
]

if __name__ == '__main__':
    m.write_macroized()
