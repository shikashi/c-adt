import macroize as m

##### ORIGINAL

m.original_name = 'vector_pod'
m.original_dir  = '../vector/vector_pod'

m.original_header = '%s/%s.h' % (m.original_dir, m.original_name)
m.original_source = '%s/%s.c' % (m.original_dir, m.original_name)

##### MACROIZATION

m.macroized_name = 'vector'
m.macroized_file = '%s_macro.h' % m.original_name

m.includes = {
    'sys': ['stdlib.h', 'stdbool.h', 'assert.h', 'string.h'],
    'lib': ['vector/vector_common.h'],
    'local': []
}

m.declaration_macro_params    = ['NAME', 'value_type']
m.implementation_macro_params = ['NAME', 'value_type',
    'DEFAULT_GROW_POLICY', 'DEFAULT_GROW_FACTOR', 'DEFAULT_VALUE']

m.common_substitutions = [
    # names (functions, types)
    ('%s_' % m.original_name, '%s_##NAME##_' % m.macroized_name),

    # the structure
    ('struct %s' % m.original_name, 'struct %s_##NAME' % m.macroized_name)
]

m.decl_substitutions = [
    # function declarations, replaced after common substitutions
    (r'^.+ %s_.+\(.*\);$' % m.macroized_name, r'QUALIF \g<0>')
]

m.impl_substitutions = [
    # the initializers for the 3 constants
    (r'kVectorPodDefaultGrowPolicy = [^\n]*;',
      'kVectorPodDefaultGrowPolicy = DEFAULT_GROW_POLICY;'),
    (r'kVectorPodDefaultGrowFactor = [^\n]*;',
      'kVectorPodDefaultGrowFactor = DEFAULT_GROW_FACTOR;'),
    (r'kVectorPodDefaultValue = [^\n]*;',
      'kVectorPodDefaultValue = DEFAULT_VALUE;'),

    # the names of the 3 constants (notice the previous sub uses the old names
    # so this must be after)
    ('kVectorPodDefaultGrowPolicy', 'kVectorPodDefaultGrowPolicy_##NAME'),
    ('kVectorPodDefaultGrowFactor', 'kVectorPodDefaultGrowFactor_##NAME'),
    ('kVectorPodDefaultValue', 'kVectorPodDefaultValue_##NAME')
]

if __name__ == '__main__':
    m.write_macroized()
