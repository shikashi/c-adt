/*
 * my_vector_value_manip.h
 *
 * Copyright (c) 2013, Sebastian Portillo <msp0111@famaf.unc.edu.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 */

#ifndef _MY_VECTOR_VALUE_MANIP_H_
#define _MY_VECTOR_VALUE_MANIP_H_

#include <vector/vector/vector.h>

/*
 * Note
 * This is separate from my_vector_types.h because:
 *   1) It's not needed in vector.h.
 *   2) By providing a separate file, it can be independently included from
 *      vector.c. In this way, we don't polute whatever context includes
 *      vector.h with extraneous names.
 */

static value_type my_new_default(void)
{
    return 0;
}

static value_type my_new_copy(value_type u)
{
    return u;
}

static void my_destroy(value_type u)
{
    (void)u;
}

/*
 * Note
 * See [1] to find out why this is a macro and not a 'constant'; in short: C
 * sucks.
 * [1] http://stackoverflow.com/questions/3025050/error-initializer-element-is-not-constant
 */
#define kMyVectorValueManip {my_new_default, my_new_copy, my_destroy}

#endif
