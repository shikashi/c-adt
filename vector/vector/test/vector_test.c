#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <assert.h>

#include <vector/vector/vector.h>

typedef uint64_t u64;
typedef __uint64_t __u64;

void test2(vector_t v)
{
    size_t i = 0;

    vector_reserve(v, 13);

    vector_resize(v, 15);

    for (i = 0; i < 20; ++i)
        vector_push_back(v, 50 + i);

    vector_erase(v, 14);
    assert(*vector_at(v, 14) == 50);

    printf("size: %lu\n", vector_size(v));

    for (i = 0; i < vector_size(v); ++i)
        printf("%d ", *vector_at(v, i));
    printf("\n");

    vector_set_grow_policy(v, VGP_ADHOC);
    vector_fit(v);

    printf("capacity: %lu\n", vector_capacity(v));

}

void test1(void)
{
    vector_t w;
    vector_t v = vector_create(10);
    test2(v);
    w = vector_copy(v);
    vector_destroy(v);
    test2(w);
    vector_destroy(w);
}

int main(void)
{
    test1();

    return 0;
}
