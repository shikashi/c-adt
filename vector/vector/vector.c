/*
 * vector.c
 *
 * Copyright (c) 2013, Sebastian Portillo <msp0111@famaf.unc.edu.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 */

#include <assert.h>
#include <stdlib.h>
#include <string.h>  /* memmove */

#include <vector/vector/vector.h>
#include <vector/vector/my_vector_value_manip.h>

/*
 * TODO: Consider making the constructor take a 'size' instead of a
 * 'capacity', ie. making it more like STL's vector.
 */

/*** CODE BEGIN ***/

struct vector {
    size_t capacity;
    size_t size;
    vgpolicy_t grow_policy;
    float grow_factor;
    value_type* arr;
};

static const vgpolicy_t kVectorDefaultGrowPolicy = VGP_PREALLOC;
static const float kVectorDefaultGrowFactor = 2.0;
static const vector_value_manip_t kVectorValueManip = kMyVectorValueManip;

static bool vector_grow(vector_t vec)
{
    assert(vec != NULL);

    switch (vec->grow_policy) {
        case VGP_PREALLOC:
            if (vector_reserve(vec, vec->capacity*vec->grow_factor))
                break;
            /* else, fall through, try to grow at least by 1 */
        case VGP_ADHOC:
            if (vector_reserve(vec, vec->capacity + 1))
                break;
            return false;  /* could not grow */
        default:
            assert(false);
    }

    return true;
}

static bool vector_shrink(vector_t vec)
{
    assert(vec != NULL);

    switch (vec->grow_policy) {
        case VGP_PREALLOC:
            return vector_reserve(vec, vec->size*vec->grow_factor);
        case VGP_ADHOC:
            return vector_reserve(vec, vec->size);
        default:
            assert(false);
    }

    return true;
}

vector_t vector_create(size_t capacity)
{
    vector_t vec = NULL;

    assert(kVectorValueManip.new_default != NULL);
    assert(kVectorValueManip.new_copy != NULL);
    assert(kVectorValueManip.destroy != NULL);

    vec = malloc(sizeof(*vec));
    if (vec == NULL)
        return NULL;

    vec->arr = malloc(capacity*sizeof(value_type));
    if (vec->arr == NULL && capacity != 0) { /* malloc failed */
        free(vec);
        return NULL;
    }

    vec->capacity = capacity;
    vec->size = 0;
    vec->grow_policy = kVectorDefaultGrowPolicy;
    vec->grow_factor = kVectorDefaultGrowFactor;

    return vec;
}

vector_t vector_copy(vector_t vec)
{
    vector_t copy = NULL;
    size_t i = 0;

    assert(vec != NULL);

    copy = vector_create(vec->capacity);
    if (copy == NULL)
        return NULL;

    copy->grow_policy = vec->grow_policy;
    copy->grow_factor = vec->grow_factor;
    for (i = 0; i < vec->size; ++i)
        copy->arr[i] = kVectorValueManip.new_copy(vec->arr[i]);
    copy->size = vec->size;

    return copy;
}

void vector_destroy(vector_t vec)
{
    size_t i;

    assert(vec != NULL);

    for (i = 0; i < vec->size; ++i)
        kVectorValueManip.destroy(vec->arr[i]);

    free(vec->arr);
    free(vec);
}

float vector_get_grow_factor(vector_t vec)
{
    assert(vec != NULL);

    return vec->grow_factor;
}

void vector_set_grow_factor(vector_t vec, float grow_factor)
{
    assert(vec != NULL);
    assert(grow_factor > 1);

    vec->grow_factor = grow_factor;
}

vgpolicy_t vector_get_grow_policy(vector_t vec)
{
    assert(vec != NULL);

    return vec->grow_policy;
}

void vector_set_grow_policy(vector_t vec, vgpolicy_t grow_policy)
{
    assert(vec != NULL);
    assert(grow_policy == VGP_ADHOC || grow_policy == VGP_PREALLOC);

    vec->grow_policy = grow_policy & VGP_MASK;
}

value_type* vector_push_back(vector_t vec, value_type val)
{
    size_t pos = 0;

    assert(vec != NULL);

    if (vec->size == vec->capacity)
        vector_grow(vec);

    pos = vec->size++;
    vec->arr[pos] = val;
    return vec->arr + pos;
}

void vector_pop_back(vector_t vec)
{
    assert(vec != NULL);
    assert(vec->size > 0);

    kVectorValueManip.destroy(vec->arr[--vec->size]);
}

value_type* vector_back(vector_t vec)
{
    assert(vec != NULL);
    assert(vec->size > 0);

    return vec->arr + (vec->size - 1);
}

size_t vector_size(vector_t vec)
{
    assert(vec != NULL);

    return vec->size;
}

size_t vector_capacity(vector_t vec)
{
    assert(vec != NULL);

    return vec->capacity;
}

value_type* vector_at(vector_t vec, size_t index)
{
    assert(vec != NULL);
    assert(index < vector_size(vec));

    return vec->arr + index;
}

void vector_foreach(vector_t vec, vector_foreach_f f)
{
    size_t i = 0;

    assert(vec != NULL);
    assert(f != NULL);

    for (i = 0; i < vec->size; ++i)
        f(i, vec->arr + i);
}

void vector_foreach_data(vector_t vec, vector_foreach_data_f f, void* data)
{
    size_t i = 0;

    assert(vec != NULL);
    assert(f != NULL);

    for (i = 0; i < vec->size; ++i)
        f(i, vec->arr + i, data);
}

void vector_foreach_func(vector_t vec, vector_foreach_func_f f, void(*func)())
{
    size_t i = 0;

    assert(vec != NULL);
    assert(f != NULL);

    for (i = 0; i < vec->size; ++i)
        f(i, vec->arr + i, func);
}

bool vector_resize(vector_t vec, size_t new_size)
{
    assert(vec != NULL);

    if (new_size > vec->capacity && !vector_reserve(vec, new_size))
        return false;

    while (new_size > vec->size)
        vector_push_back(vec, kVectorValueManip.new_default());

    while (new_size < vec->size)
        vector_pop_back(vec);

    return true;
}

bool vector_reserve(vector_t vec, size_t new_capacity)
{
    value_type* new_arr = NULL;

    assert(vec != NULL);

    if (new_capacity <= vec->capacity) /* already have that reserved */
        return true;

    new_arr = realloc(vec->arr, new_capacity*sizeof(value_type));
    if (new_arr == NULL && new_capacity != 0)  /* realloc failed */
        return false;

    vec->arr = new_arr;
    vec->capacity = new_capacity;

    return true;
}

void vector_erase(vector_t vec, size_t index)
{
    assert(vec != NULL);
    assert(index < vector_size(vec));

    kVectorValueManip.destroy(vec->arr[index]);

    if (index != vec->size - 1)
        memmove(vec->arr + index, vec->arr + index + 1,
                (vec->size - index - 1)*sizeof(value_type));

    --vec->size;
}

void vector_clear(vector_t vec)
{
    size_t i;

    assert(vec != NULL);

    for (i = 0; i < vec->size; ++i)
        kVectorValueManip.destroy(vec->arr[i]);
    vec->size = 0;
}

void vector_fit(vector_t vec)
{
    assert(vec != NULL);

    vector_shrink(vec);
}

/*** CODE END ***/
