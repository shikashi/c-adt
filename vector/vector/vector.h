/*
 * vector.h
 *
 * Copyright (c) 2013, Sebastian Portillo <msp0111@famaf.unc.edu.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 */

#ifndef _VECTOR_H_
#define _VECTOR_H_

#include <stdbool.h>

#include <vector/vector_common.h>
#include <vector/vector/my_vector_types.h>

/*** CODE BEGIN ***/

/**
 * Callback function used when iterating over the vector, where,
 *   i      The elements' index.
 *   v      The elements' value.
 */
typedef void (*vector_foreach_f)(size_t i, value_type* v);

/**
 * Callback function used when iterating over the vector, where,
 *   i      The elements' index.
 *   v      The elements' value.
 *   d      Opaque object pointer reserved for the user's contextual data.
 */
typedef void (*vector_foreach_data_f)(size_t i, value_type* v, void* d);

/**
 * Callback function used when iterating over the vector, where,
 *   i      The elements' index.
 *   v      The elements' value.
 *   f      Opaque function pointer reserved for the user's contextual data.
 */
typedef void (*vector_foreach_func_f)(size_t i, value_type* v, void(*f)());

/**
 * Callback functions used by the vector to manipulate the elements it
 * contains.
 */
typedef struct {
    /**
     * Default constructor.
     */
    value_type (*new_default)(void);

    /**
     * Copy constructor.
     */
    value_type (*new_copy)(value_type);

    /**
     * Destructor.
     */
    void (*destroy)(value_type);
} vector_value_manip_t;

typedef struct vector* vector_t;

/**
 * Constructor.
 * Construct a vector with initial capacity 'capacity'.
 * The capacity of a vector is the maximum amount of elements that can be added
 * with the certainty that the vector will not incur in reallocs. This, in
 * turn, assures that pointers into the array will not be invalidated.
 */
vector_t vector_create(size_t capacity);

/**
 * Copy constructor.
 *
 * Pre: vec != NULL
 */
vector_t vector_copy(vector_t vec);

/**
 * Destructor.
 * All remaining elements are destroyed.
 *
 * Pre: vec != NULL
 */
void vector_destroy(vector_t vec);

/**
 * Return the vector's grow factor.
 * When the vector needs to grow, and the current grow policy is VGP_PREALLOC,
 * it does so by this factor.
 *
 * Pre: vec != NULL && vector_get_grow_factor(vec) > 1
 */
float vector_get_grow_factor(vector_t vec);

/**
 * Set the vector's grow factor.
 * When the vector needs to grow, and the current grow policy is VGP_PREALLOC,
 * it does so by this factor.
 *
 * Pre: vec != NULL && grow_factor > 1
 */
void vector_set_grow_factor(vector_t vec, float grow_factor);

/**
 * Return the vector's grow policy.
 * When the vector needs to grow it does so according to the current policy:
 *   VGP_ADHOC      The vector grows one element at a time.
 *   VGP_PREALLOC   The vector grows by a configurable factor of its current
 *                  size.
 *
 * Pre: vec != NULL
 */
vgpolicy_t vector_get_grow_policy(vector_t vec);

/**
 * Set the vector's grow policy.
 * When the vector needs to grow it does so according to the current policy:
 *   VGP_ADHOC      The vector grows one element at a time.
 *   VGP_PREALLOC   The vector grows by a configurable factor of its current
 *                  size.
 *
 * Pre: vec != NULL && grow_policy & VGP_MASK
 */
void vector_set_grow_policy(vector_t vec, vgpolicy_t grow_policy);

/**
 * Append 'val' to the end of the vector, resizing it if necessary.
 * Return a poiter to the appended element.
 *
 * Pre: vec != NULL
 */
value_type* vector_push_back(vector_t vec, value_type val);

/**
 * Remove the last element from vector.
 * The removed element is destructed.
 *
 * Pre: vec != NULL && vector_size(vec) > 0
 */
void vector_pop_back(vector_t vec);

/**
 * Return a pointer to the last element in the vector.
 *
 * Pre: vec != NULL && vector_size(vec) > 0
 */
value_type* vector_back(vector_t vec);

/**
 * Return the vector's current size.
 *
 * Pre: vec != NULL
 */
size_t vector_size(vector_t vec);

/**
 * Return the vector's current capacity.
 * The capacity of a vector is the maximum amount of elements that can be added
 * with the certainty that the vector will not incur in reallocs. This, in
 * turn, assures that pointers into the array will not be invalidated.
 *
 * Pre: vec != NULL
 */
size_t vector_capacity(vector_t vec);

/**
 * Return a pointer to the element with position 'index'.
 *
 * Pre: vec != NULL && index < vector_size(vec)
 * Post: vector_at(vec, index) != NULL
 */
value_type* vector_at(vector_t vec, size_t index);

/**
 * Call 'f' for each element in the vector, in order, from lower to higher
 * index.
 *
 * Pre: vec != NULL && f != NULL
 */
void vector_foreach(vector_t vec, vector_foreach_f f);

/**
 * Call 'f' for each element in the vector, in order, from lower to higher
 * index. Pass 'data' along to 'f'.
 *
 * Pre: vec != NULL && f != NULL
 */
void vector_foreach_data(vector_t vec, vector_foreach_data_f f, void* data);

/**
 * Call 'f' for each element in the vector, in order, from lower to higher
 * index. Pass 'data' along to 'f'.
 *
 * Pre: vec != NULL && f != NULL
 */
void vector_foreach_func(vector_t vec, vector_foreach_func_f f, void(*func)());

/**
 * Change the size of the vector to 'new_size'.
 * If the size is decreased, elements are removed from the end and destroyed.
 * If the size is increased, default-constructed elements are appended.
 * Note that increasing the size can invalidate pointers into the vector.
 * Return true if successful; otherwise false.
 *
 * Pre: vec != NULL
 */
bool vector_resize(vector_t vec, size_t new_size);

/**
 * Change the capacity of the vector to 'new_capacity'.
 * This function can only increase the capacity.
 * The capacity of a vector is the maximum amount of elements that can be added
 * with the certainty that the vector will not incur in reallocs. This, in
 * turn, assures that pointers into the array will not be invalidated.
 * Note that increasing the capacity can invalidate pointers into the vector.
 *
 * Pre: vec != NULL
 */
bool vector_reserve(vector_t vec, size_t new_capacity);

/**
 * Remove from the vector the element at position 'index'.
 * The removed element is destroyed.
 * Note that is potentially a costly operation.
 *
 * Pre: vec != NULL && index < vector_size(index)
 */
void vector_erase(vector_t vec, size_t index);

/**
 * Remove and destroy all elements from the vector.
 * Note that this does not reduce the vector's capacity.
 *
 * Pre: vec != NULL
 * Post: vector_size(vec) == 0
 */
void vector_clear(vector_t vec);

/**
 * Decrease the vector's capacity.
 * The amount of memory waived back in this manner depends on the current grow
 * policy.
 *
 * Pre: vec != NULL
 */
void vector_fit(vector_t vec);

/*** CODE END ***/

#endif
