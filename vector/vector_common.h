/*
 * vector_common.h
 *
 * Copyright (c) 2013, Sebastian Portillo <msp0111@famaf.unc.edu.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 */

#ifndef _VECTOR_COMMON_H_
#define _VECTOR_COMMON_H_

typedef enum {
    VGP_ADHOC = 0,
    VGP_PREALLOC = 1,
    VGP_MASK = 1
} vgpolicy_t;

#endif
