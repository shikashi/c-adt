#include <assert.h>

#include "vector_entryptr.h"

entry_t entry_default(void)
{
    entry_t e = malloc(sizeof(*e));
    if (e == NULL)
        return NULL;

    e->key = 20;
    e->val = 50;
    e->erased = false;
    e->next = NULL;

    return e;
}

entry_t entry_copy(entry_t e)
{
    entry_t copy = NULL;

    assert(e != NULL);

    copy = malloc(sizeof(*copy));
    if (copy == NULL)
        return NULL;

    *copy = *e;
    copy->next = NULL;

    return copy;
}

void entry_destroy(entry_t e)
{
    free(e);
}

#define ENTRY_MANIP {entry_default, entry_copy, entry_destroy}

IMPLEMENT_VECTOR(entryptr, entry_t, VGP_PREALLOC, 2.0, ENTRY_MANIP)
