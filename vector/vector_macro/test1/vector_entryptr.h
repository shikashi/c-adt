#ifndef _VECTOR_ENTRYPTR_H_
#define _VECTOR_ENTRYPTR_H_

#include <stdint.h>
#include <stdbool.h>

#include <vector/vector_macro/vector_macro.h>

typedef struct entry* entry_t;

struct entry {
    uint32_t key;
    size_t val;
    bool erased;
    entry_t next;
};

entry_t entry_default(void);
entry_t entry_copy(entry_t e);
void entry_destroy(entry_t e);

DECLARE_VECTOR(entryptr, entry_t)

#endif
