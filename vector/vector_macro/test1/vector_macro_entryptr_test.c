#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include "vector_entryptr.h"

void test2(vector_entryptr_t v)
{
    size_t i;

    vector_entryptr_reserve(v, 13);

    vector_entryptr_resize(v, 15);

    for (i = 0; i < 20; ++i) {
        entry_t e = entry_default();
        e->key = i + 100;
        e->val = i;
        vector_entryptr_push_back(v, e);
    }

    printf("size: %lu\n", vector_entryptr_size(v));

    for (i = 0; i < vector_entryptr_size(v); ++i) {
        entry_t e = *vector_entryptr_at(v, i);
        printf("(%d,%lu,%d,%p)  ", e->key, e->val, e->erased, (void*)e->next);
    }
    printf("\n");

    vector_entryptr_set_grow_policy(v, VGP_ADHOC);
    vector_entryptr_fit(v);

    printf("capacity: %lu\n", vector_entryptr_capacity(v));
}

void test1(void)
{
    vector_entryptr_t w;
    vector_entryptr_t v = vector_entryptr_create(10);
    test2(v);
    w = vector_entryptr_copy(v);
    vector_entryptr_destroy(v);
    test2(w);
    vector_entryptr_destroy(w);
}

int main(void)
{
    test1();

    return 0;
}
