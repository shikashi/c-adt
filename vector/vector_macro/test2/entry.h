#ifndef _ENTRY_H_
#define _ENTRY_H_

#include <stdint.h>
#include <stdbool.h>

typedef struct entry* entry_t;

struct entry {
    uint32_t key;
    size_t val;
    bool erased;
    entry_t next;
};

entry_t entry_default(void);
entry_t entry_copy(entry_t e);
void entry_destroy(entry_t e);

#define ENTRY_MANIP {entry_default, entry_copy, entry_destroy}

#endif
