/*
 * my_vector_pod_default_value.h
 *
 * Copyright (c) 2013, Sebastian Portillo <msp0111@famaf.unc.edu.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 */

#ifndef _MY_VECTOR_POD_DEFAULT_VALUE_H_
#define _MY_VECTOR_POD_DEFAULT_VALUE_H_

#include <vector/vector_pod/vector_pod.h>

/*
 * Note
 * This is separate from my_vector_pod_types.h because:
 *   1) It's not needed in vector_pod.h.
 *   2) By providing a separate file, it can be independently included from
 *      vector_pod.c. In this way, we don't polute whatever context includes
 *      vector_pod.h with extraneous names.
 */

/*
 * Note
 * See [1] to find out why this is a macro and not a 'constant'; in short: C
 * sucks.
 * [1] http://stackoverflow.com/questions/3025050/error-initializer-element-is-not-constant
 */
#define kMyVectorPodDefaultValue 0

#endif
