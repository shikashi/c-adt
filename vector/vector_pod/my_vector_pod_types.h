/*
 * my_vector_pod_types.h
 *
 * Copyright (c) 2013, Sebastian Portillo <msp0111@famaf.unc.edu.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 */

#ifndef _MY_VECTOR_POD_TYPES_H_
#define _MY_VECTOR_POD_TYPES_H_

typedef unsigned value_type;

#endif
