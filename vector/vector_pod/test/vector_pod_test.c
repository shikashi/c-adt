#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <assert.h>

#include <vector/vector_pod/vector_pod.h>

typedef uint32_t u32;
typedef __uint64_t u64;

void test2(vector_pod_t v)
{
    size_t i = 0;

    vector_pod_reserve(v, 13);

    vector_pod_resize(v, 15);

    for (i = 0; i < 20; ++i)
        vector_pod_push_back(v, 50 + i);

    vector_pod_erase(v, 14);
    assert(*vector_pod_at(v, 14) == 50);

    printf("size: %lu\n", vector_pod_size(v));

    for (i = 0; i < vector_pod_size(v); ++i)
        printf("%d ", *vector_pod_at(v, i));
    printf("\n");

    vector_pod_set_grow_policy(v, VGP_ADHOC);
    vector_pod_fit(v);

    printf("capacity: %lu\n", vector_pod_capacity(v));
}

void test1(void)
{
    vector_pod_t w;
    vector_pod_t v = vector_pod_create(10);
    test2(v);
    w = vector_pod_copy(v);
    vector_pod_destroy(v);
    test2(w);
    vector_pod_destroy(w);
}

int main(void)
{
    test1();

    return 0;
}
