/*
 * vector_pod.c
 *
 * Copyright (c) 2013, Sebastian Portillo <msp0111@famaf.unc.edu.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 */

#include <assert.h>
#include <stdlib.h>
#include <string.h>  /* memmove */

#include <vector/vector_pod/vector_pod.h>
#include <vector/vector_pod/my_vector_pod_default_value.h>

/*
 * TODO: Consider making the constructor take a 'size' instead of a
 * 'capacity', ie. making it more like STL's vector.
 */

/*** CODE BEGIN ***/

struct vector_pod {
    size_t capacity;
    size_t size;
    vgpolicy_t grow_policy;
    float grow_factor;
    value_type* arr;
};

static const vgpolicy_t kVectorPodDefaultGrowPolicy = VGP_PREALLOC;
static const float kVectorPodDefaultGrowFactor = 2.0;
static const value_type kVectorPodDefaultValue = kMyVectorPodDefaultValue;

static bool vector_pod_grow(vector_pod_t vec)
{
    assert(vec != NULL);

    switch (vec->grow_policy) {
        case VGP_PREALLOC:
            if (vector_pod_reserve(vec, vec->capacity*vec->grow_factor))
                break;
            /* else, fall through, try to grow at least by 1 */
        case VGP_ADHOC:
            if (vector_pod_reserve(vec, vec->capacity + 1))
                break;
            return false;  /* could not grow */
        default:
            assert(false);
    }

    return true;
}

static bool vector_pod_shrink(vector_pod_t vec)
{
    assert(vec != NULL);

    switch (vec->grow_policy) {
        case VGP_PREALLOC:
            return vector_pod_reserve(vec, vec->size*vec->grow_factor);
        case VGP_ADHOC:
            return vector_pod_reserve(vec, vec->size);
        default:
            assert(false);
    }

    return true;
}

vector_pod_t vector_pod_create(size_t capacity)
{
    vector_pod_t vec = NULL;

    vec = malloc(sizeof(*vec));
    if (vec == NULL)
        return NULL;

    vec->arr = malloc(capacity*sizeof(value_type));
    if (vec->arr == NULL && capacity != 0) { /* malloc failed */
        free(vec);
        return NULL;
    }

    vec->capacity = capacity;
    vec->size = 0;
    vec->grow_policy = kVectorPodDefaultGrowPolicy;
    vec->grow_factor = kVectorPodDefaultGrowFactor;

    return vec;
}

vector_pod_t vector_pod_copy(vector_pod_t vec)
{
    vector_pod_t copy = NULL;
    size_t i = 0;

    assert(vec != NULL);

    copy = vector_pod_create(vec->capacity);
    if (copy == NULL)
        return NULL;

    copy->grow_policy = vec->grow_policy;
    copy->grow_factor = vec->grow_factor;
    for (i = 0; i < vec->size; ++i)
        copy->arr[i] = vec->arr[i];
    copy->size = vec->size;

    return copy;
}

void vector_pod_destroy(vector_pod_t vec)
{
    assert(vec != NULL);

    free(vec->arr);
    free(vec);
}

float vector_pod_get_grow_factor(vector_pod_t vec)
{
    assert(vec != NULL);

    return vec->grow_factor;
}

void vector_pod_set_grow_factor(vector_pod_t vec, float grow_factor)
{
    assert(vec != NULL);
    assert(grow_factor > 1);

    vec->grow_factor = grow_factor;
}

vgpolicy_t vector_pod_get_grow_policy(vector_pod_t vec)
{
    assert(vec != NULL);

    return vec->grow_policy;
}

void vector_pod_set_grow_policy(vector_pod_t vec, vgpolicy_t grow_policy)
{
    assert(vec != NULL);
    assert(grow_policy == VGP_ADHOC || grow_policy == VGP_PREALLOC);

    vec->grow_policy = grow_policy & VGP_MASK;
}

value_type* vector_pod_push_back(vector_pod_t vec, value_type val)
{
    size_t pos = 0;

    assert(vec != NULL);

    if (vec->size == vec->capacity)
        vector_pod_grow(vec);

    pos = vec->size++;
    vec->arr[pos] = val;
    return vec->arr + pos;
}

void vector_pod_pop_back(vector_pod_t vec)
{
    assert(vec != NULL);
    assert(vec->size > 0);

    --vec->size;
}

value_type* vector_pod_back(vector_pod_t vec)
{
    assert(vec != NULL);
    assert(vec->size > 0);

    return vec->arr + (vec->size - 1);
}

size_t vector_pod_size(vector_pod_t vec)
{
    assert(vec != NULL);

    return vec->size;
}

size_t vector_pod_capacity(vector_pod_t vec)
{
    assert(vec != NULL);

    return vec->capacity;
}

value_type* vector_pod_at(vector_pod_t vec, size_t index)
{
    assert(vec != NULL);
    assert(index < vector_pod_size(vec));

    return vec->arr + index;
}

void vector_pod_foreach(vector_pod_t vec, vector_pod_foreach_f f)
{
    size_t i = 0;

    assert(vec != NULL);
    assert(f != NULL);

    for (i = 0; i < vec->size; ++i)
        f(i, vec->arr + i);
}

void vector_pod_foreach_data(vector_pod_t vec, vector_pod_foreach_data_f f,
        void* data)
{
    size_t i = 0;

    assert(vec != NULL);
    assert(f != NULL);

    for (i = 0; i < vec->size; ++i)
        f(i, vec->arr + i, data);
}

void vector_pod_foreach_func(vector_pod_t vec, vector_pod_foreach_func_f f,
        void(*func)())
{
    size_t i = 0;

    assert(vec != NULL);
    assert(f != NULL);

    for (i = 0; i < vec->size; ++i)
        f(i, vec->arr + i, func);
}

bool vector_pod_resize(vector_pod_t vec, size_t new_size)
{
    assert(vec != NULL);

    if (new_size > vec->capacity && !vector_pod_reserve(vec, new_size))
        return false;

    while (new_size > vec->size)
        vector_pod_push_back(vec, kVectorPodDefaultValue);

    while (new_size < vec->size)
        vector_pod_pop_back(vec);

    return true;
}

bool vector_pod_reserve(vector_pod_t vec, size_t new_capacity)
{
    value_type* new_arr = NULL;

    assert(vec != NULL);

    if (new_capacity <= vec->capacity) /* already have that reserved */
        return true;

    new_arr = realloc(vec->arr, new_capacity*sizeof(value_type));
    if (new_arr == NULL && new_capacity != 0)  /* realloc failed */
        return false;

    vec->arr = new_arr;
    vec->capacity = new_capacity;

    return true;
}

void vector_pod_erase(vector_pod_t vec, size_t index)
{
    assert(vec != NULL);
    assert(index < vector_pod_size(vec));

    if (index != vec->size - 1)
        memmove(vec->arr + index, vec->arr + index + 1,
                (vec->size - index - 1)*sizeof(value_type));

    --vec->size;
}

void vector_pod_clear(vector_pod_t vec)
{
    assert(vec != NULL);

    vec->size = 0;
}

void vector_pod_fit(vector_pod_t vec)
{
    assert(vec != NULL);

    vector_pod_shrink(vec);
}

/*** CODE END ***/
