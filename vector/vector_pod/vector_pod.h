/*
 * vector_pod.h
 *
 * Copyright (c) 2013, Sebastian Portillo <msp0111@famaf.unc.edu.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 */

#ifndef _VECTOR_POD_H_
#define _VECTOR_POD_H_

#include <stdbool.h>

#include <vector/vector_common.h>
#include <vector/vector/my_vector_types.h>

/*** CODE BEGIN ***/

/**
 * This is a specialization of 'vector' intended to be used with POD elements.
 * As such, contained element are not 'constructed' nor 'destroyed', and
 * copying is performed by simple byte mirroring.
 * When needed, for instance when resizing, a user-configured 'default'
 * element is used.
 */

/**
 * Callback function used when iterating over the vector, where,
 *   i      The elements' index.
 *   v      The elements' value.
 */
typedef void (*vector_pod_foreach_f)(size_t i, value_type* v);

/**
 * Callback function used when iterating over the vector, where,
 *   i      The elements' index.
 *   v      The elements' value.
 *   d      Opaque object pointer reserved for the user's contextual data.
 */
typedef void (*vector_pod_foreach_data_f)(size_t i, value_type* v, void* d);

/**
 * Callback function used when iterating over the vector, where,
 *   i      The elements' index.
 *   v      The elements' value.
 *   f      Opaque function pointer reserved for the user's contextual data.
 */
typedef void (*vector_pod_foreach_func_f)(size_t i, value_type* v, void(*f)());

typedef struct vector_pod* vector_pod_t;

/**
 * Constructor.
 * Construct a vector with initial capacity 'capacity'.
 * The capacity of a vector is the maximum amount of elements that can be added
 * with the certainty that the vector will not incur in reallocs. This, in
 * turn, assures that pointers into the array will not be invalidated.
 */
vector_pod_t vector_pod_create(size_t capacity);

/**
 * Copy constructor.
 *
 * Pre: vec != NULL
 */
vector_pod_t vector_pod_copy(vector_pod_t vec);

/**
 * Destructor.
 *
 * Pre: vec != NULL
 */
void vector_pod_destroy(vector_pod_t vec);

/**
 * Return the vector's grow factor.
 * When the vector needs to grow, and the current grow policy is VGP_PREALLOC,
 * it does so by this factor.
 *
 * Pre: vec != NULL && vector_pod_get_grow_factor(vec) > 1
 */
float vector_pod_get_grow_factor(vector_pod_t vec);

/**
 * Set the vector's grow factor.
 * When the vector needs to grow, and the current grow policy is VGP_PREALLOC,
 * it does so by this factor.
 *
 * Pre: vec != NULL && grow_factor > 1
 */
void vector_pod_set_grow_factor(vector_pod_t vec, float grow_factor);

/**
 * Return the vector's grow policy.
 * When the vector needs to grow it does so according to the current policy:
 *   VGP_ADHOC      The vector grows one element at a time.
 *   VGP_PREALLOC   The vector grows by a configurable factor of its current
 *                  size.
 *
 * Pre: vec != NULL
 */
vgpolicy_t vector_pod_get_grow_policy(vector_pod_t vec);

/**
 * Set the vector's grow policy.
 * When the vector needs to grow it does so according to the current policy:
 *   VGP_ADHOC      The vector grows one element at a time.
 *   VGP_PREALLOC   The vector grows by a configurable factor of its current
 *                  size.
 *
 * Pre: vec != NULL && grow_policy & VGP_MASK
 */
void vector_pod_set_grow_policy(vector_pod_t vec, vgpolicy_t grow_policy);

/**
 * Append 'val' to the end of the vector, resizing it if necessary.
 * Return a poiter to the appended element.
 *
 * Pre: vec != NULL
 */
value_type* vector_pod_push_back(vector_pod_t vec, value_type val);

/**
 * Remove the last element from vector.
 * The removed element is destructed.
 *
 * Pre: vec != NULL && vector_pod_size(vec) > 0
 */
void vector_pod_pop_back(vector_pod_t vec);

/**
 * Return a pointer to the last element in the vector.
 *
 * Pre: vec != NULL && vector_pod_size(vec) > 0
 */
value_type* vector_pod_back(vector_pod_t vec);

/**
 * Return the vector's current size.
 *
 * Pre: vec != NULL
 */
size_t vector_pod_size(vector_pod_t vec);

/**
 * Return the vector's current capacity.
 * The capacity of a vector is the maximum amount of elements that can be added
 * with the certainty that the vector will not incur in reallocs. This, in
 * turn, assures that pointers into the array will not be invalidated.
 *
 * Pre: vec != NULL
 */
size_t vector_pod_capacity(vector_pod_t vec);

/**
 * Return a pointer to the element with position 'index'.
 *
 * Pre: vec != NULL && index < vector_pod_size(vec)
 * Post: vector_pod_at(vec, index) != NULL
 */
value_type* vector_pod_at(vector_pod_t vec, size_t index);

/**
 * Call 'f' for each element in the vector, in order, from lower to higher
 * index.
 *
 * Pre: vec != NULL && f != NULL
 */
void vector_pod_foreach(vector_pod_t vec, vector_pod_foreach_f f);

/**
 * Call 'f' for each element in the vector, in order, from lower to higher
 * index. Pass 'data' along to 'f'.
 *
 * Pre: vec != NULL && f != NULL
 */
void vector_pod_foreach_data(vector_pod_t vec, vector_pod_foreach_data_f f,
        void* data);

/**
 * Call 'f' for each element in the vector, in order, from lower to higher
 * index. Pass 'data' along to 'f'.
 *
 * Pre: vec != NULL && f != NULL
 */
void vector_pod_foreach_func(vector_pod_t vec, vector_pod_foreach_func_f f,
        void(*func)());

/**
 * Change the size of the vector to 'new_size'.
 * If the size is decreased, elements are removed from the end.
 * If the size is increased, default elements are appended.
 * Note that increasing the size can invalidate pointers into the vector.
 * Return true if successful; otherwise false.
 *
 * Pre: vec != NULL
 */
bool vector_pod_resize(vector_pod_t vec, size_t new_size);

/**
 * Change the capacity of the vector to 'new_capacity'.
 * This function can only increase the capacity.
 * The capacity of a vector is the maximum amount of elements that can be added
 * with the certainty that the vector will not incur in reallocs. This, in
 * turn, assures that pointers into the array will not be invalidated.
 * Note that increasing the capacity can invalidate pointers into the vector.
 *
 * Pre: vec != NULL
 */
bool vector_pod_reserve(vector_pod_t vec, size_t new_capacity);

/**
 * Remove from the vector the element at position 'index'.
 * Note that is potentially a costly operation.
 *
 * Pre: vec != NULL && index < vector_pod_size(index)
 */
void vector_pod_erase(vector_pod_t vec, size_t index);

/**
 * Remove all elements from the vector.
 * Note that this does not reduce the vector's capacity.
 *
 * Pre: vec != NULL
 * Post: vector_pod_size(vec) == 0
 */
void vector_pod_clear(vector_pod_t vec);

/**
 * Decrease the vector's capacity.
 * The amount of memory waived back in this manner depends on the current grow
 * policy.
 *
 * Pre: vec != NULL
 */
void vector_pod_fit(vector_pod_t vec);

/*** CODE END ***/

#endif
