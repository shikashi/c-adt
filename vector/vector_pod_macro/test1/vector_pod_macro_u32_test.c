#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include "vector_u32.h"

void test2(vector_u32_t v)
{
    size_t i = 0;

    vector_u32_reserve(v, 13);

    vector_u32_resize(v, 15);

    for (i = 0; i < 20; ++i)
        vector_u32_push_back(v, i);

    printf("size: %lu\n", vector_u32_size(v));

    for (i = 0; i < vector_u32_size(v); ++i)
        printf("%d ", *vector_u32_at(v, i));
    printf("\n");

    vector_u32_set_grow_policy(v, VGP_ADHOC);
    vector_u32_fit(v);

    printf("capacity: %lu\n", vector_u32_capacity(v));

}

void test1(void)
{
    vector_u32_t w;
    vector_u32_t v = vector_u32_create(10);
    test2(v);
    w = vector_u32_copy(v);
    vector_u32_destroy(v);
    test2(w);
    vector_u32_destroy(w);
}

int main(void)
{
    test1();

    return 0;
}
