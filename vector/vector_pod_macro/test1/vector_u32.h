#ifndef _VECTOR_U32_H_
#define _VECTOR_U32_H_

#include <stdint.h>

#include <vector/vector_pod_macro/vector_pod_macro.h>

typedef uint32_t u32;

DECLARE_VECTOR_POD(u32, u32)

#endif
