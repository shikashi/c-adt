#include <assert.h>

#include "vector_entry.h"

#define ENTRY_DEFAULT {0, 0, false, NULL}

IMPLEMENT_VECTOR_POD(entry, struct entry, VGP_PREALLOC, 2.0, ENTRY_DEFAULT)
