#ifndef _VECTOR_ENTRY_H_
#define _VECTOR_ENTRY_H_

#include <stdint.h>
#include <stdbool.h>

#include <vector/vector_pod_macro/vector_pod_macro.h>

typedef struct entry* entry_t;

struct entry {
    uint32_t key;
    size_t val;
    bool erased;
    entry_t next;
};

DECLARE_VECTOR_POD(entry, struct entry)

#endif
