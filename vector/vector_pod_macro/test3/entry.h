#ifndef _ENTRY_H_
#define _ENTRY_H_

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

typedef struct entry* entry_t;

struct entry {
    uint32_t key;
    size_t val;
    bool erased;
    entry_t next;
};

#define ENTRY_DEFAULT {0, 0, false, NULL}

#endif
