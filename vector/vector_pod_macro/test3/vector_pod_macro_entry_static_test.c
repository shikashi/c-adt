#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include <vector/vector_pod_macro/vector_pod_macro.h>
#include "entry.h"

DECLARE_STATIC_VECTOR_POD(entry, struct entry)
IMPLEMENT_VECTOR_POD(entry, struct entry, VGP_PREALLOC, 2.0, ENTRY_DEFAULT)

void test2(vector_entry_t v)
{
    size_t i;
    struct entry e;

    vector_entry_reserve(v, 13);

    vector_entry_resize(v, 15);

    for (i = 0; i < 20; ++i) {
        e.key = i + 100;
        e.val = i;
        e.erased = false;
        e.next = NULL;
        vector_entry_push_back(v, e);
    }

    printf("size: %lu\n", vector_entry_size(v));

    for (i = 0; i < vector_entry_size(v); ++i) {
        entry_t e = vector_entry_at(v, i);
        printf("(%d,%lu,%d,%p)  ", e->key, e->val, e->erased, (void*)e->next);
    }
    printf("\n");

    vector_entry_set_grow_policy(v, VGP_ADHOC);
    vector_entry_fit(v);

    printf("capacity: %lu\n", vector_entry_capacity(v));
}

void test1(void)
{
    vector_entry_t w;
    vector_entry_t v = vector_entry_create(10);
    test2(v);
    w = vector_entry_copy(v);
    vector_entry_destroy(v);
    test2(w);
    vector_entry_destroy(w);
}

int main(void)
{
    test1();

    return 0;
}
